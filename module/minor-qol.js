"use strict";

import { AbilityTemplate } from "../../../systems/dnd5e/module/pixi/ability-template.js";
var knownSheets = undefined;

// Strings to look for in chatmessages
var attackRollFlavor;
var damageRollFlavor;
var savingThrowText;
// Spells that do no damage when saved against.
let noDamageSaves = [];

let debug = false;
let log = (...args) => console.log("Minor Qol | ", ...args);

const MESSAGETYPES = {
    hitData: 1,
    saveData: 2
};

function resetStatus(user, versatile=false) {
setProperty(user.data.flags, "minor-qol", 
  { "hitTargets": game.user.targets, 
    isCrtical: false, 
    isFumble: false, 
    saves: new Set(), 
    versatile: versatile
  });
}

let hotbarHandler = (bar, data, slot) => {
  console.warn("Hotbar Hook fired", bar, data, slot);

  if(debug) log("Hotbar Hook fired", bar, data, slot);
  if (!macroSpeedRolls) return true;
  if (data.type !== "Item") return true;
  if (debug) log("hotbar drop", "Creating macro");
  createMinorQOLMacro(data.data, slot);
  return false;
}

export function initSetup() {
    // get all the game.settings.("minor-qol", ...)
    fetchParams();
    // Watch for minor-qol hidden names and reveal them to the GM
    setupHiddenNameButtons();
      // when fixed replace with Hooks.on("hotbarDrop", hotbarHandler)
    Hooks._hooks.hotbarDrop = [hotbarHandler].concat(Hooks._hooks.hotbarDrop || []);
}

export function readySetup() {
  setProperty(game.user.data.flags, "minor-qol.hideSaves", false);
  if (debug) log("Setup entered")
  if (!knownSheets) {
      console.error("Minor-qol | known sheets not setup - module not installed");
      return;
  }
  for (let sheetName of Object.keys(knownSheets)) {
      Hooks.on("render" + sheetName, enableSheetQOL);
  }
  Hooks.on("renderedAlt5eSheet", enableSheetQOL);

  // When a measured template is display auto target actors it auto target set
  Hooks.on("createMeasuredTemplate", selectTargets);
  // respond to chat rolls (attack and damage trigger further actions)
  setupRollHandling();

  // need to wait for translations to be loaded before setting these
  attackRollFlavor = i18n("minor-qol.attackRollFlavorText");
  damageRollFlavor = i18n("minor-qol.damageRollFlavorText");
  savingThrowText = i18n("minor-qol.savingThrow");
}

let itemRollButtons, speedItemRolls, autoTarget;
let useHigherSlot, autoCheckHit, autoCheckSaves, autoRollDamage;
let addChatDamageButtons, autoApplyDamage, damageImmunities;
let macroSpeedRolls, hideNPCNames, itemDeleteCheck;

export let fetchParams = () => {
  itemRollButtons = game.settings.get("minor-qol", "ItemRollButtons");
  speedItemRolls = game.settings.get("minor-qol", "SpeedItemRolls");
  autoTarget = game.settings.get("minor-qol", "AutoTarget");
  useHigherSlot = game.settings.get("minor-qol", "UseHigherSlot");
  autoCheckHit = game.settings.get("minor-qol", "AutoCheckHit");
  autoCheckSaves = game.settings.get("minor-qol", "AutoCheckSaves");
  autoRollDamage = game.settings.get("minor-qol", "AutoRollDamage");
  addChatDamageButtons = game.settings.get("minor-qol", "AddChatDamageButtons");
  autoApplyDamage = game.settings.get("minor-qol", "AutoApplyDamage");
  damageImmunities = game.settings.get("minor-qol", "DamageImmunities");
  macroSpeedRolls = game.settings.get("minor-qol", "MacroSpeedRolls");
  hideNPCNames = game.settings.get("minor-qol", "HideNPCNames");
  itemDeleteCheck = game.settings.get("minor-qol", "ItemDeleteCheck");
}

/**
 * If we are auto checking saves disable the display of created saves - checkSaves will display a combo card instead
 * @param {[message]} messages 
 * @param {} data 
 * @param {} options 
 */
let processpreCreateSaveRoll = (messages, data, options) => {
  if (!getProperty(game.user.data.flags, "minor-qol.hideSaves")) return true;
  if (!game.settings.get("minor-qol", "AutoCheckSaves")) return true;
  if (data.user !== game.user.id) return true;
  options.displaySheet = false;
  return false;
}

let processPreCreateDamageRoll = async (messages, data, options) => {
  if (debug) log("processpreCreateDamageRoll", messages, data, data.user);
  // if (data.user != game.user._id) return; not required only creator gets this message
  if (!autoCheckSaves) return true;
    let actor = game.actors.tokens[data.speaker.token];
    if (!actor) actor = game.actors.get(data.speaker.actor);
    let item = actor.items.find(i => data.flavor.startsWith(`${i.name}${damageRollFlavor}`));
    let saves = new Set();
    if (autoCheckSaves) saves = await checkSaves(game.user.targets, item);
    setProperty(game.user.data.flags, "minor-qol.saves", saves)
  return true;
}

/**
 * Set handlers for attack and damage rolls
 */
let setupRollHandling = () => {
  Hooks.on("preCreateChatMessage", (messages, data, options) => {
    if (debug) log("preCreateChatMessage hook", messages, data, options);
    if (data.flavor && data.flavor.includes(damageRollFlavor)) return processPreCreateDamageRoll(messages, data, options);
    if (data.flavor && data.flavor.includes(savingThrowText)) return processpreCreateSaveRoll(messages, data, options);
    if (data.flavor && data.flavor.includes(attackRollFlavor)) return processPreCreateAttackRoll(messages, data, options);
  });

  Hooks.on("renderChatMessage", (message, html, data) => {
      if (!message.isRoll) return true;
      if (debug) log("renderChatMessage hook", message, html, data, attackRollFlavor, damageRollFlavor, message.data.flavor);
      let flavor = message.data.flavor;
      if (flavor && flavor.includes(damageRollFlavor)) return processDamageRoll(message, html, data);
      return true;
  });
};

/**
 * 
 * @param {Item} item 
 * @param {integer} slot 
 * 
 * If no macro exists create a macro roll
 */
async function createMinorQOLMacro(item, slot) {
    const command = `MinorQOL.doRoll(event, "${item.name}", {type: "${item.type}", versatile: false});`;
    let macro = game.macros.entities.find(m => m.name.startsWith(item.name)  &&  m.data.command === command);
    if (!macro) {
        macro = await Macro.create({
            name: `${item.name} - ${item.type}`,
            type: "script",
            img: item.img,
            command: command,
            flags: { "dnd5e.itemMacro": true }
        }, { displaySheet: false });
    }
    game.user.assignHotbarMacro(macro, slot);
}

export function doMacroRoll(event, itemName, itemType = "weapon") {
  return doRoll(event, itemName, {type: itemType});
}

export function doRoll(event, itemName, {type = "weapon", versatile=false}={}) {
    let actor;
    if (!macroSpeedRolls) return game.dnd5e.rollItemMacro(itemName);
    // Get the syntehtic actor if there is one
    if (canvas.tokens.controlledTokens.length > 0) {
        actor = canvas.tokens.controlledTokens[0].actor;
    }
    if (!actor)
        actor = game.actors.get(ChatMessage.getSpeaker().actor);
    if (!actor) {
      ui.notifications.warn(`${game.i18n.localize("minor-qol.noSelection")}`)
      return;
    } 
    if (!event.originalEvent) {
      // from macros we get a mouse event so create a false event with the right shift key behaviour
      event = {
          altKey: event.altKey,
          shiftKey: event.shiftKey,
          metaKey: event.metaKey,
          ctrlKey: event.ctrlKey,
          originalEvent: event
      };
    }
    
    const item = actor ? actor.items.find(i => i.name === itemName && (!type || i.type === type)) : null;
    if (!item)
        return ui.notifications.warn(`${i18n("minor-qol.noItemNamed")} ${itemName}`);
    if (item.hasAttack || item.hasSave || item.hasDamage) {
        return doCombinedRoll({actor, item, event, consumeSlot: true, versatile});
    }
    if (item.type === "spell")
        return actor.useSpell(item, { configureDialog: !event.shiftKey });
    if (item.type === "tool") {
        setShiftOnly(event);
        return item.rollToolCheck({ event });
    }
    if (item.type === "consumable") {
      // TODO - consume the consumable.
        setShiftOnly(event);
        return rollConsumable(event, actor, item);
    }
    return item.roll({ configureDialog: !event.shiftKey });
}

let enableSheetQOL = (app, html, data) => {
    // find out how to reinstate the original handler later.
    const defaultTag = ".item .item-image";
    //Add a check for item deletion
    if (itemDeleteCheck) {
        // remove current handler - this is a bit clunky since it results in a case with no delete handler
        $(html).find(".item-delete").off("click");
        $(html).find(".item-delete").click({ app: app, data: data }, itemDeleteHandler);
    }

    let rollTag = knownSheets[app.constructor.name] ? knownSheets[app.constructor.name] : defaultTag;
    if (itemRollButtons)
        addItemSheetButtons(app, html, data);
    if (speedItemRolls) {
        // Item Rolling do attack and damge at the same
        $(html).find(rollTag).off("click");
        $(html).find(rollTag).click({ app, data, html }, itemRollHandler);
    }
};


function addItemSheetButtons(app, html, data, triggeringElement = "", buttonContainer = "") {
    // Setting default element selectors
    if (triggeringElement === "")
        triggeringElement = ".item .item-name h4";
    if (buttonContainer === "")
        buttonContainer = ".item-properties";

    // adding an event for when the description is shown
    html.find(triggeringElement).click(event => {
        let li = $(event.currentTarget).parents(".item");
        let item = app.object.getOwnedItem(li.attr("data-item-id"));
        let actor = app.object;
        let chatData = item.getChatData();
        if (!li.hasClass("expanded"))
            return; // this is a way to not continue if the items description is not shown, but its only a minor gain to do this while it may break this module in sheets that dont use "expanded"
        let buttonsWereAdded = false;
        // Create the buttons
        let buttons = $(`<div class="item-buttons"></div>`);
        switch (item.data.type) {
            case "weapon":
            case "spell":
            case "feat":
                if (speedItemRolls)
                    buttons.append(`<span class="tag"><button data-action="basicRoll">${i18n("minor-qol.buttons.roll")}</button></span>`);
                if (item.hasAttack)
                    buttons.append(`<span class="tag"><button data-action="attack">${i18n("minor-qol.buttons.attack")}</button></span>`);
                if (item.hasDamage)
                    buttons.append(`<span class="tag"><button data-action="damage">${i18n("minor-qol.buttons.damage")}</button></span>`);
                if (item.isVersatile) 
                    buttons.append(`<span class="tag"><button data-action="versatileAttack">${i18n("minor-qol.buttons.versatileAttack")}</button></span>`);
                if (item.isVersatile) 
                  buttons.append(`<span class="tag"><button data-action="versatileDamage">${i18n("minor-qol.buttons.versatileDamage")}</button></span>`);
//                if (speedItemRolls && item.hasSave && item.hasDamage)
//                    buttons.append(`<span class="tag"><button data-action="damage">${i18n("minor-qol.buttons.saveAndDamage")}</button></span>`);
                buttonsWereAdded = true;
                break;
            case "consumable":
                if (chatData.hasCharges)
                    buttons.append(`<span class="tag"><button data-action="consume">${i18n("minor-qol.buttons.itemUse")} ${item.name}</button></span>`);
                buttonsWereAdded = true;
                break;
            case "tool":
                buttons.append(`<span class="tag"><button data-action="toolCheck" data-ability="${chatData.ability.value}">${i18n("minor-qol.buttons.itemUse")} ${item.name}</button></span>`);
                buttonsWereAdded = true;
                break;
        }

        if (buttonsWereAdded) {
            buttons.append(`<br><header style="margin-top:6px"></header>`);
            // adding the buttons to the sheet
            let targetHTML = $(event.target.parentNode.parentNode);
            targetHTML.find(buttonContainer).prepend(buttons);
            buttons.find("button").click({ app: app, data: data, html: html }, ev => {
                ev.preventDefault();
                ev.stopPropagation();
                if (debug) log("roll handler ", ev.target.dataset.action)
                // If speed rolls are off
                switch (ev.target.dataset.action) {
                    case "attack":
                        resetStatus(game.user);
                        item.rollAttack({ event: ev });
                        break;
                    case "versatileAttack":
                        resetStatus(game.user);
                        setProperty(game.user.data.flags, "minor-qol.versatile", true);
                          item.rollAttack({ event: ev });
                        break;
                    case "damage":
                        item.rollDamage({ event: ev, versatile: false });
                        break;
                    case "versatileDamage":
                        item.rollDamage({ event: ev, versatile: true });
                        break;
                    case "consume":
                        item.roll({ event: ev });
                        break;
                    case "toolCheck":
                        item.rollToolCheck({ event: ev });
                        break;
                    case "basicRoll":
                        if (item.type === "spell") {
                          setProperty(game.user.data.flags, "minor-qol.hitTargets", game.user.targets);
                          actor.useSpell(item, { configureDialog: !ev.shiftKey });
                        }
                        else
                            item.roll();
                        break;
                }
            });
        }
    });
}

function itemRollHandler(event, consumeSlot = true) {
  // Allow shift/ctl/alt from the weapon img - unshifted works as before
  if (event.__proto__.preventDefault)
      event.preventDefault();
  //let actor = game.actors.get(event.data.data.actor._id);
  let actor;
  // If the app has a token then this is a token sheet and we want the actor inside the token
  if (event.data.app.token)
      actor = event.data.app.token.actor;
  else if (event.data.app.object)
      actor = event.data.app.object;
  // this should be defined
  else
      actor = game.actors.get(event.data.data.actor._id); // but just in case we can get the global Actor if we must
  let itemId = $(event.currentTarget).parents(".item").attr("data-item-id");
  let item = actor.getOwnedItem(itemId);
  if (debug) log("itemRollhandler", itemId, item, actor)
  if (speedItemRolls) {
    if (item.hasAttck || item.hasSave || item.hasDamage) {
        return doCombinedRoll({actor, item, event, consumeSlot});
    }
    if (item.type === "spell")
      return actor.useSpell(item, { configureDialog: !event.shiftKey });
    if (item.type === "tool") {
      setShiftOnly(event);
      return item.rollToolCheck({ event: event });
    }
    if (item.type === "consumable") {
      setShiftOnly(event);
      item.rollConsumable({event});
      // return rollConsumable({item, event});
    }
  }
  return item.roll({event});
}

let rollConsumable = ({item, event}) => {
    let quantity = item.data.data.quantity;
    if (quantity > 0) {
        item.rollConsumable({ event });
    }
    if (quantity === 1) {
        item.update({ "data.quantity": 0 });
    }
};

/**
 * Calculate the level of a cast spell. If consuming slots it will be the level of the spell (if there are any slots available)
 * unless usesHigherSlot is true, when it will search for the first available slot to cast at.
 * The actor is updated to reflect the slot consumption.
 * @param {Item} item 
 * @param {Actor actor 
 * @param {boolean} consumeSlot 
 */
let calcSlot = async (item, actor, consumeSlot) => {
  // speed rolls use default level
  let lvl = item.data.data.level;
  if (consumeSlot) {
    const usesSlots = lvl > 0 && ["prepared", "always", "pact"].includes(item.data.data.preparation.mode);
    if (usesSlots) {
      let newSlots = -1;
      while (newSlots < 0 && lvl <= 9) {
          newSlots = Math.max(parseInt(actor.data.data.spells["spell" + lvl].value) - 1, -1);
          if (!useHigherSlot)
              break;
          // higher level slot
          if (newSlots < 0)
              lvl = lvl + 1;
      }
      if (newSlots < 0) {
          log(`${i18n("minor-qol.NoSlots")}`);
          ui.notifications.info(i18n("minor-qol.NoSlots"));
          return -1;
      } else {
          await actor.update({ [`data.spells.spell${lvl}.value`]: newSlots });
      }
    }
  }
  return lvl;
}

// Fires on renderMeasuredTemplate.
// set game user targets
let selectTargets = (scene, sceneId, template, embedded, userId) => {
  let targeting = autoTarget;
  if (targeting === "none")
      return true;
  if (userId !== game.user._id) {
      return true;
  }
  
  if (template) {
    // release current targets
    game.user.targets.forEach(t => {
      t.setTarget(false, { releaseOthers: false });
    });
    game.user.targets.clear();
  }

  let wallsBlockTargeting = targeting === "wallsBlock";
  let templateDetails = canvas.templates.get(template._id);
  let tdx = templateDetails.data.x;
  let tdy = templateDetails.data.y;
  canvas.tokens.placeables.filter(t => {
    if (!t.actor) return false;
    if (!templateDetails.shape.contains(t.x + t.w / 2 - tdx, t.y + t.h / 2 - tdy))
      return false;
    if (!wallsBlockTargeting)
      return true;
    // construct a ray and check for collision
    let r = new Ray({ x: t.x + t.w / 2, y: t.y + t.h / 2 }, templateDetails.data);
    return !canvas.walls.checkCollision(r);
  }).forEach(t => {
    t.setTarget(true, { user: game.user, releaseOthers: false });
    game.user.targets.add(t);
  });

  // Assumes area affect do not have a to hit roll
  setProperty(game.user.data.flags, "minor-qol.saves", new Set());
  setProperty(game.user.data.flags, "minor-qol.hitTargets", game.user.targets);

  Hooks.callAll("minor-qol-targeted", game.user.targets);
  return true;
};

let doCombinedRoll = async ({actor, item, event, consumeSlot = true, versatile=false}) => {
  // stage 1 - do spell casting requirements.
  if (debug) log("docombinedRoll", actor, item, event);
  log("docombinedRoll", actor, item, event, versatile);

  // Initiate ability template placement workflow if selected
  // setProperty(game.user.data.flags, "minor-qol.hitTargets", game.user.targets);

  resetStatus(game.user, versatile);
  // If this is a spell, 
  // 1. Make sure there are spell slots to cast and consume spell slot.
  // record the spell level so the damage roll will work.
  // 2. If it is a template roll, draw the template and use that to tirgger setting targets
  // 3. Cause finish roll to be called either directly or after the template placement
  
  let spellLevel;
  if (item.type === "spell")
    if(consumeSlot) {
      spellLevel = await calcSlot(item, actor, consumeSlot)
    if (spellLevel === -1) return;
  }
  setProperty(game.user.data.flags, "minor-qol.spellLevel", spellLevel);
  if (item.hasAreaTarget) {
    // hide the character sheet if displayed
    if (actor.sheet.rendered) actor.sheet.minimize();
    const template = AbilityTemplate.fromItem(item);
    // drawing the template removes all selected tokens - so remmber which one to get the right synthectic actor later.
    let token = canvas.tokens.controlledTokens[0];
    // When the target is placed finish the roll
    Hooks.once("minor-qol-targeted", finishRoll.bind(null, {actor, item, event, token}));
    template.drawPreview(event);
    // don't continue the roll until the template is placed
    return false;
  }
  if (debug) log("about to call finished roll")
  return finishRoll({ actor, item, event});
};

let consumeCharge = async (item) => {
  let itemData = item.data.data;
  if ( itemData.uses.autoUse ) {
    let q = itemData.quantity;
    let c = itemData.uses.value;
    // Deduct an item quantity
    if ( c <= 1 && q > 1 ) {
      await item.update({
        'data.quantity': Math.max(q - 1, 0),
        'data.uses.value': itemData.uses.max
      });
    }
    // Optionally destroy the item
    else if ( c <= 1 && q <= 1 && itemData.uses.autoDestroy ) {
      await item.actor.deleteOwnedItem(this.id);
    }
    // Deduct the remaining charges
    else {
      await item.update({'data.uses.value': Math.max(c - 1, 0)});
    }
  }
}

/**
 * 
 * @param {Item, Event} Item that is rolling, event that generated the roll.
 * @param {*} targets optional list of targets to roll against
 */
let finishRoll = async ({ actor, item, event, token=null }, targets = null) => {
  // Spell has been cast if it is one.
  if (debug) log("finishRoll", item, event, token);
  // Next do the attack Roll

  // if a token is passed we need to take control of it - else we will get the wrong speaker sent to the damage card
  if (token) token.control({ releaseOthers: true });

  if (!event.originalEvent) {
    // from macros we get a mouse event so create a false event with the right shift key behaviour
    event = {
        altKey: event.altKey,
        shiftKey: event.shiftKey,
        metaKey: event.metaKey,
        ctrlKey: event.ctrlKey,
        originalEvent: event
    };
  }
  if (!(event.altKey || event.shiftKey || event.metaKey || event.ctrlKey)) {
      setShiftOnly(event);
  }

  if (item.hasAttack) {
      if (!(event.altKey || event.shiftkey || event.ctrlKey)) setShiftOnly(event);
      // item.rollAttack({ event, versatile: getProperty(game.user.data.flags, "minor-qol.versatible") });
      item.rollAttack({ event });
      return true;
  }

  if (item.type === "consumable") { // these need to be consumed first.
    if (item.hasDamage) await consumeCharge(item);
    else return item.rollConsumable({event});
  }
  
  if (item.hasDamage && autoRollDamage) {
    item.rollDamage({ 
      event, 
      spellLevel: getProperty(game.user.data.flags, "minor-qol.spellLevel"), 
      versatile: getProperty(game.user.data.flags, "minor-qol.versatile")
    });
  }
  return true;
};

let processPreCreateAttackRoll =  async (messages, data, options) => {
  let theTargets = game.user.targets;
  let isCritical, isFumble;
  let isHit = true;
  let attackRoll, attackTotal, actor, item;
  if (debug) log("preprocessAttackRoll |", speedItemRolls, autoCheckHit, autoApplyDamage)
  if (speedItemRolls || autoCheckHit) {
    attackRoll = Roll.fromData(JSON.parse(data.roll));
    attackTotal = attackRoll.total;
    actor = game.actors.tokens[data.speaker.token];
    if (!actor) actor = game.actors.get(data.speaker.actor);
    item = actor.items.find(i => i.hasAttack && data.flavor.startsWith(`${i.name}${attackRollFlavor}`));
  }

  // Assume we have a single die result
  isCritical = attackRoll.parts[0].results[0] >= attackRoll.parts[0].options.critical;
  isFumble = attackRoll.parts[0].results[0] <= attackRoll.parts[0].options.fumble;
        
  if (autoCheckHit) {
    // let theTargets = game.user.targets;
    let msg = "";
    let sep = "";
    
    // check for a hit/critical/fumble
    isHit = false;
    theTargets = new Set();
    let hitDisplay = [];

    for (let targetToken of game.user.targets) {
      let targetActor = targetToken.actor;
      if (!isFumble && !isCritical) {
          // check to see if the roll hit the target
          let targetAC = targetActor.data.data.attributes.ac.value;
          if (game.user.isGM) log(`${data.speaker.alias} Rolled a ${attackTotal} to hit ${targetActor.name}s AC of ${targetAC}`);
          isHit = attackTotal >= targetAC;
      }
      // Log the hit on the target
      let attackType = item.type === "spell" ? i18n(item.name) : i18n(item.name);
      let hitString = isCritical ? i18n("minor-qol.criticals") : isFumble? i18n("minor-qol.fumbles") : isHit ? i18n("minor-qol.hits") : i18n("minor-qol.misses");
      hitDisplay.push({isPC: targetToken.actor.isPC, target: targetToken, hitString, attackType});

      // If we hit and we have targets and we are applying damage say so.
      if (isHit || isCritical) theTargets.add(targetToken);
    }
    let templateData = {
      hits: hitDisplay, 
      isGM: game.user.isGM,
      damageAppliedString: autoApplyDamage && theTargets.size > 0 && autoRollDamage ? i18n("minor-qol.damage-applied") : ""
    }
    let content = await renderTemplate("modules/minor-qol/templates/hits.html", templateData);
    if (game.user.targets.size > 0) {
      ChatMessage.create({
          user: game.user._id,
          speaker: { actor: actor, alias: actor.name },
          content: content,
          type: CONST.CHAT_MESSAGE_TYPES.OTHER,
          flags: { minorQolType: MESSAGETYPES.hitData }
      });
    }
  }
  setProperty(game.user.data.flags, "minor-qol.hitTargets", theTargets);
  let shouldRollDamage = item.hasDamage && autoRollDamage && !isFumble && (
                      (game.user.targets.size === 0) || // nothing selected so roll damage if auto enabled
                      (!autoCheckHit) || // we are not checking for hits so roll damage
                      (theTargets.size > 0)); // we actually hit something
  if (debug) log("process attack roll - roll damage?", shouldRollDamage, item, item.hasDamage, autoRollDamage, game.user.targets.size, autoCheckHit, theTargets.size);
  let event = {};
  if (isCritical)
      setAltOnly(event);
  else
      setShiftOnly(event);
  // Chain the damage roll if required
  if (shouldRollDamage) {
    if (debug) log("About to roll daamage")
    item.rollDamage({ 
      event, 
      spellLevel: getProperty(game.user.data.flags, "minor-qol.spellLevel"), 
      versatile: getProperty(game.user.data.flags, "minor-qol.versatile")
    });
  }
  return true;
};

/**
 * If autoApplyDamge is set apply the damage to minor-qol.hitTargets using immunities and saving throws if appropriate.
 * If addDamageButtons add damage buttons to the roll
 * 
 * @param {ChatMessage} message 
 * @param {*} html 
 * @param {*  } data 
 */
let processDamageRoll = async (message, html, data) => {
  if (debug) log("processDamageRoll", message, message.data.user);
  // proceed ifadding chat damage buttons or applying datme for our selves
  if (!addChatDamageButtons && !(autoApplyDamage && message.data.user ===  game.user._id)) {
    resetStatus(game.user);
   return true;
  }

  let actor = game.actors.tokens[message.data.speaker.token];
  if (!actor) actor = game.actors.get(message.data.speaker.actor);
  let item = actor.items.find(i => message.data.flavor.startsWith(`${i.name}${damageRollFlavor}`) && i.hasDamage);
  let totalDamage = message.roll.total;
  let damageDetail = createDamageList(message.roll, item);
  let theTargets = getProperty(game.user.data.flags, "minor-qol.hitTargets");
  let saves = getProperty(game.user.data.flags, "minor-qol.saves");

  if (addChatDamageButtons )
    addDamageButtons(damageDetail, totalDamage, html);
  if (debug) log("processDamageRoll - autoapplydamage", autoApplyDamage, "game.user.id", game.user._id, "message.data.user.id", message.data.user)
  if (autoApplyDamage && game.user._id === message.data.user) {
    applyTokenDamage(damageDetail, totalDamage, theTargets, item, saves);
  }
  resetStatus(game.user)
  return true;
};

/**
 * 
 * @param {Set} theTargets 
 * @param {Item} item 
 * 
 * Return a Set of successful saves from the set of tokens theTargets.
 */
let checkSaves = async (theTargets, item) => {
    let saves = new Set();
    let saveDisplay = [];
    if (item.hasSave && theTargets.size > 0) {
        theTargets = game.user.targets;
        setProperty(game.user.data.flags, "minor-qol.hitTargets", game.user.targets);
        let rollDC = item.data.data.save.dc;
        let rollAbility = item.data.data.save.ability;
        let message = "";
        let promises = [];
        // make sure saving throws are renabled.
        try {
          setProperty(game.user.data.flags, "minor-qol.hideSaves", true);
          for (let target of theTargets) {
            promises.push(target.actor.rollAbilitySave(item.data.data.save.ability, { event: { shiftKey: true } }));
          }
          if (debug) log("Results are ", results);
          } catch (err) {
        } finally {
          setProperty(game.user.data.flags, "minor-qol.hideSaves", false);
        }

        let results = await Promise.all(promises);
        let i = 0;
        for (let target of theTargets) {
            let rollTotal = results[i].total;
            let saved = rollTotal >= rollDC;
            if (saved)
                saves.add(target);
            if (game.user.isGM) log(`Ability save result is ${target.name} rolled ${rollTotal} vs ${rollAbility} DC ${rollDC}`);
            let saveString = i18n(saved ? "minor-qol.save-success" : "minor-qol.save-failure");
            let noDamage = saved && getSaveMultiplierForSpell(item) === 0 ? i18n("minor-qol.noDamage") : "";
            saveDisplay.push({name: target.name, img: target.data.img, isPC: target.actor.isPC, target, saveString, rollTotal, noDamage, id: target.id});
            i++;
        }
        let templateData = {
          saves: saveDisplay, 
          damageAppliedString: autoApplyDamage ? i18n("minor-qol.damage-applied") : ""
         }
        let content = await renderTemplate("modules/minor-qol/templates/saves.html", templateData);

        ChatMessage.create({
            user: game.user._id,
            speaker: { actor: item.actor, alias: item.actor.name },
            content: content,
            flavor: `<h4">DC ${rollDC} ${CONFIG.DND5E.abilities[rollAbility]} ${i18n(theTargets.size > 1 ? "minor-qol.saving-throws" : "minor-qol.saving-throw")}:</h4>`,
            type: CONST.CHAT_MESSAGE_TYPES.OTHER,
            flags: { minorQolType: MESSAGETYPES.saveData }
        });
    }
    return saves;
};

// Calculate the hp/tempHP lost for an amount of damage of type
function calculateDamage(a, appliedDamage, t, totalDamage) {
  let value = Math.floor(appliedDamage);
  let hp = a.data.data.attributes.hp, tmp = parseInt(hp.temp) || 0, dt = value > 0 ? Math.min(tmp, value) : 0;
  let newTemp = tmp - dt;
  let oldHP = hp.value;
  let newHP = Math.clamped(hp.value - (value - dt), 0, hp.max);
  if (game.user.isGM)
      log(`${a.name} takes ${value} reduced from ${totalDamage} Temp HP ${newTemp} HP ${newHP}`);
  return {tokenID: t.id, actorID: a._id, tempDamage: dt, hpDamage: oldHP - newHP, oldTempHP: tmp, newTempHP: newTemp,
          oldHP: oldHP, newHP: newHP, totalDamage: totalDamage, appliedDamage: value};
}


/** 
 * Work out the appropriate multiplier for DamageTypeString on actor
 * If DamageImmunities are not being checked always return 1
 * 
 */

let getTraitMult = (actor, dmgTypeString) => {
  if (damageImmunities) {
    if (dmgTypeString !== "") {
      if (actor.data.data.traits.di.value.includes(dmgTypeString)) return 0;
      if (actor.data.data.traits.dr.value.includes(dmgTypeString)) return 0.5;
      if (actor.data.data.traits.dv.value.includes(dmgTypeString)) return 2;
    }
    if (dmgTypeString === "healing") return -1;
    // Check the custom immunities
  }
  return 1;
};

let _highlighted = null;

let _onTargetHover = (event) => {
  event.preventDefault();
  if ( !canvas.scene.data.active ) return;
//  const li = event.currentTarget;
//  const token = canvas.tokens.get(li.id);
  const token = canvas.tokens.get(event.currentTarget.id);
  if ( token && token.isVisible ) {
    if ( !token._controlled ) token._onMouseOver(event);
    _highlighted = token;
  }
}

/* -------------------------------------------- */

/**
 * Handle mouse-unhover events for a combatant in the tracker
 * @private
 */
let _onTargetHoverOut = (event) => {
  event.preventDefault();
  if ( !canvas.scene.data.active ) return;
  if (_highlighted ) _highlighted._onMouseOut(event);
  _highlighted = null;
}

let _onTargetSelect = (event) => {
  event.preventDefault();
  if ( !canvas.scene.data.active ) return;
  const token = canvas.tokens.get(event.currentTarget.id);
  token.control({ multiSelect: false, releaseOthers: true });
};


/**
 * If this is a minor qol roll (save or hits) make the actor name buttons select the token for the GM.
 */
let setupHiddenNameButtons = () => {
  Hooks.on("renderChatMessage", (message, html, data) => {
    if (![MESSAGETYPES.hitData, MESSAGETYPES.saveData].includes(getProperty(message.data.flags, "minorQolType")))
      return;
    let ids = html.find(".minor-qol-target-name")
    // let buttonTargets = html.getElementsByClassName("minor-qol-target-npc");
    ids.hover(_onTargetHover, _onTargetHoverOut)

    if (game.user.isGM)  {
      ids.click(_onTargetSelect);
    }
    if (!game.user.isGM && hideNPCNames) {
      ids=html.find(".minor-qol-target-npc");
      ids.text("-???-");
    }
  });
}

/**
 *  return a list of {damage: number, type: string} for the roll and the item
 */
let createDamageList = (roll, item) => {
  let damageList = [];
  let damageSpec = item.data.data.damage;
  let rollParts = roll.parts;
  let partPos = 0;
  let evalString;
  for (let [spec, type] of damageSpec.parts) {
      let specLength = spec.split(/\+|-|\*|\//).length;
      evalString = "";
      for (let i = 0; i < specLength; i++) {
          if (partPos === 0) {
              evalString = rollParts[0];
              if (typeof evalString !== "string")
                  evalString = `${rollParts[0].results.reduce((a, b) => a + b)}`;
              partPos += 1;
              continue;
          }
          evalString += rollParts[partPos];
          let evalTerm = rollParts[partPos + 1];
          if (typeof evalTerm !== "string") {
              evalTerm = `${evalTerm.results.reduce((a, b) => a + b)}`;
          }
          evalString += evalTerm;
          partPos += 2;
      }
      let damage = eval(evalString);
      damageList.push({ damage: damage, type: type });
  }
  evalString = "";
  while (partPos < rollParts.length) {
      evalString += rollParts[partPos];
      let evalTerm = rollParts[partPos + 1];
      if (typeof evalTerm !== "string") {
          evalTerm = `${evalTerm.total}`;
      }
      evalString += evalTerm;
      partPos += 2;
  }
  if (evalString.length > 0) {
      let damage = eval(evalString);
      damageList.push({ damage: damage, type: damageSpec.parts[0][1] });
  }
  return damageList;
};
let applyTokenDamage = (damageDetail, totalDamage, theTargets, item, saves) => {
    let damageList = [];
    if (debug) log("applyTokenDame - targets", theTargets)
    if (!theTargets) {
      // probably called from refresh - don't do anything
      return true;
    }
    for (let t of theTargets) {
        let a = t.actor;
        let appliedDamage = 0;
        for (let { damage, type } of damageDetail) {
            let mult = saves.has(t) ? getSaveMultiplierForSpell(item) : 1;
            mult = mult * getTraitMult(a, type);
            appliedDamage += Math.floor(damage * Math.abs(mult)) * Math.sign(mult);
        }
        damageList.push(calculateDamage(a, appliedDamage, t, totalDamage));
    }
    if (theTargets.size > 0) {
      let intendedGM = game.users.entities.find(u => u.isGM && u.active);
      if (!intendedGM) {
        ui.notifications.error(`${game.user.name} ${i18n("minor-qol.noGM")}`);
        console.error("Minor Qol | No GM user connected - cannot apply damage");
      }
      broadcastData({
            action: "reverseDamageCard",
            sender: game.user.name,
            intendedFor: intendedGM,
            damageList: damageList
        });
    }
};

let addDamageButtons = async (damageDetail, totalDamage, html) => {
    const btnContainer = $('<span class="dmgBtn-container" style="position:absolute; right:0; bottom:1px;"></span>');
    let btnStyling = "width: 22px; height:22px; font-size:10px;line-height:1px";
    const fullDamageButton = $(`<button class="dice-total-full-damage-button" style="${btnStyling}"><i class="fas fa-user-minus" title="Click to apply full damage to selected token(s)."></i></button>`);
    const halfDamageButton = $(`<button class="dice-total-half-damage-button" style="${btnStyling}"><i class="fas fa-user-shield" title="Click to apply half damage to selected token(s)."></i></button>`);
    const doubleDamageButton = $(`<button class="dice-total-double-damage-button" style="${btnStyling}"><i class="fas fa-user-injured" title="Click to apply double damage to selected token(s)."></i></button>`);
    const fullHealingButton = $(`<button class="dice-total-full-healing-button" style="${btnStyling}"><i class="fas fa-user-plus" title="Click to apply full healing to selected token(s)."></i></button>`);
    btnContainer.append(fullDamageButton);
    btnContainer.append(halfDamageButton);
    btnContainer.append(doubleDamageButton);
    btnContainer.append(fullHealingButton);
    html.find(".dice-total").append(btnContainer);
    // Handle button clicks
    let setButtonClick = (buttonID, mult) => {
        let button = html.find(buttonID);
        button.off("click");
        button.click(async (ev) => {
            ev.stopPropagation();
            if (canvas.tokens.controlledTokens.length === 0) {
                console.warn(`Minor-qol | user ${game.user.name} ${i18n("minor-qol.noTokens")}`);
                return ui.notifications.warn(`${game.user.name} ${i18n("minor-qol.noTokens")}`);
            }
            // find solution for non-magic weapons
            let promises = [];
            for (let t of canvas.tokens.controlledTokens) {
                let a = t.actor;
                let appliedDamage = 0;
                for (let { damage, type } of damageDetail) {
                    let typeMult = mult * getTraitMult(a, type);
                    appliedDamage += Math.floor(damage * Math.abs(typeMult)) * Math.sign(typeMult);
                }
                let damageItem = calculateDamage(a, appliedDamage, t, totalDamage);
                promises.push(a.update({ "data.attributes.hp.temp": damageItem.newTempHP, "data.attributes.hp.value": damageItem.newHP }));
            }
            let retval = await Promise.all(promises);
            return retval;
        });
    };
    setButtonClick(".dice-total-full-damage-button", 1);
    setButtonClick(".dice-total-half-damage-button", 0.5);
    setButtonClick(".dice-total-double-damage-button", 2);
    setButtonClick(".dice-total-full-healing-button", -1);
};

let itemDeleteHandler = ev => {
  let actor = game.actors.get(ev.data.data.actor._id);
  let d = new Dialog({
      // localize this text
      title: i18n("minor-qol.reallyDelete"),
      content: `<p>${i18n("minor-qol.sure")}</p>`,
      buttons: {
          one: {
              icon: '<i class="fas fa-check"></i>',
              label: "Delete",
              callback: () => {
                  let li = $(ev.currentTarget).parents(".item"), itemId = li.attr("data-item-id");
                  ev.data.app.object.deleteOwnedItem(itemId);
                  li.slideUp(200, () => ev.data.app.render(false));
              }
          },
          two: {
              icon: '<i class="fas fa-times"></i>',
              label: "Cancel",
              callback: () => { }
          }
      },
      default: "two"
  });
  d.render(true);
};

let setShiftOnly = event => {
    event.shiftKey = true;
    event.altKey = false;
    event.ctrlKey = false;
    event.metaKey = false;
};
let setAltOnly = event => {
    event.shiftKey = false;
    event.altKey = true;
    event.ctrlKey = false;
    event.metaKey = false;
};

let moduleSocket = "module.minor-qol";
let createReverseDamageCard = async (data) => {
    let whisperText = "";
    const damageList = data.damageList;
    const btnStyling = "width: 22px; height:22px; font-size:10px;line-height:1px";
    let token, actor;
    const timestamp = Date.now();
    let sep = "";
    let promises = [];
    for (let { tokenID, actorID, tempDamage, hpDamage, mult, oldTempHP, newTempHP, oldHP, newHP, totalDamage, appliedDamage } of damageList) {
        token = canvas.tokens.get(tokenID);
        actor = token.actor;
        if (data.intendedFor._id === game.user._id) {
            promises.push(actor.update({ "data.attributes.hp.temp": newTempHP, "data.attributes.hp.value": newHP }));
        }
        let buttonID = `${timestamp}-${token.id}`;
        let btntxt = `<button id="${buttonID}"style="${btnStyling}"><i class="fas fa-user-plus" title="Click to reverse damage."></i></button>`;
        let tokenName = token.name ? `<strong>${token.name}</strong>` : token.actor.name;
        let dmgSign = appliedDamage < 0 ? "+" : "-"; // negative damage is added to hit points
        if (oldTempHP > 0)
            whisperText = whisperText.concat(`${sep}${duplicate(btntxt)} ${tokenName}<br> (${oldHP}:${oldTempHP}) ${dmgSign} ${Math.abs(appliedDamage)}[${totalDamage}] -> (${newHP}:${newTempHP})`);
        else
            whisperText = whisperText.concat(`${sep}${duplicate(btntxt)} ${tokenName}<br> ${oldHP} ${dmgSign} ${Math.abs(appliedDamage)}[${totalDamage}] -> ${newHP}`);
        ["di", "dv", "dr"].forEach(trait => {
          if (actor.data.data.traits[trait].custom) {
            whisperText = whisperText.concat(`<br>${trait}: ${actor.data.data.traits[trait].custom}`);
          }
        });
        sep = "<br>";
    }
    let whisperRecipients = (["0.5.1", "0.5.0"].includes(game.data.version)) ? ChatMessage.getWhisperIDs("GM") : ChatMessage.getWhisperRecipients("GM");
    let message = await ChatMessage.create({
        user: game.user._id,
        speaker: { actor: actor, alias: actor.name },
        content: whisperText,
        whisper: whisperRecipients,
        flavor: `${i18n("minor-qol.undoDamageFrom")} ${data.sender}`,
        type: CONST.CHAT_MESSAGE_TYPES.OTHER
    });
    for (let { tokenID, actorID, tempDamage, hpDamage, oldTempHP, newTemHPp, oldHP, newHP, totalDamage, appliedDamage } of damageList) {
      token = canvas.tokens.get(tokenID);
      actor = token.actor;
      let buttonID = `${timestamp}-${token.id}`;
      let button = document.getElementById(buttonID);

      button.addEventListener("click", async (ev) => {
          log(`Setting HP back to ${oldTempHP} and ${oldHP}`);
          let actor = canvas.tokens.get(tokenID).actor;
          await actor.update({ "data.attributes.hp.temp": oldTempHP, "data.attributes.hp.value": oldHP });
          ev.stopPropagation();
      });
    }
    await Promise.all(promises);
};
let processAction = data => {
    switch (data.action) {
        case "reverseDamageCard":
            if (!game.user.isGM)
                break;
            if (!autoApplyDamage)
                break;
            createReverseDamageCard(data);
            break;
    }
};
let setupSocket = () => {
    game.socket.on(moduleSocket, data => {
        processAction(data);
    });
};

function broadcastData(data) {
    // if not a gm broadcast the message to a gm who can apply the damage
    if (!game.user.isGM)
      game.socket.emit(moduleSocket, data, resp => { });
    else
      processAction(data);
}

Hooks.once("ready", () => {
    setupSocket();
    noDamageSaves = i18n("minor-qol.noDamageonSaveSpells");
});

let getSaveMultiplierForSpell = item => {
  // find a better way for this ? perhaps item property
  return noDamageSaves.includes(item.name) ? 0 : 0.5;
};

let i18n = key => {
  return game.i18n.localize(key);
};

knownSheets = {
  BetterNPCActor5eSheet: ".item .rollable",
  ActorSheet5eCharacter: ".item .item-image",
  DynamicActorSheet5e: ".item .item-image",
  ActorSheet5eNPC: ".item .item-image",
  Sky5eSheet: ".item .item-image",
};

/**
 * Application initialization
 */
Hooks.once("init", () => {
      /*
     * Key is the app name of the sheet.
     * value is the css class to identify the image/entry to attach the roll funtion to.
     *   It can be the image associated with the weapon/spell but does not need to be, anything unique will probably be fine.
     *   Hooks are added for rendering all of the sheets so you can have multiple ones active at the same time if you so wish.
     * */
 });
 
Hooks.once("setup", () => {
    MinorQOL = {
        doCombinedRoll: doCombinedRoll,
        doMacroRoll: doMacroRoll,
        doRoll: doRoll
    };
});
