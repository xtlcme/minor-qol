"use strict";
import {fetchParams} from "./minor-qol.js"
export const MODULE_NAME = 'minor-qol';

/**
 * The module title
 */
export const title = "Minor QOL settings";

/**
 * Some generic path references that might be useful later in the application's windows
 */
export const path = {
  root: `modules/${MODULE_NAME}/`,
  rollTemplate: `modules/${MODULE_NAME}/templates/roll.html`
};

/**
 * For each setting, there is are two corresponding entries in the language file to retrieve the translations for
 * - the setting name
 * - the hint displayed beneath the setting's name in the "Configure Game Settings" dialog.
 *
 * Given your MODULE_NAME is 'my-module' and your setting's name is 'EnableCritsOnly', then you will need to create to language entries:
 * {
 *  "my-module.EnableCritsOnly.Name": "Enable critical hits only",
 *  "my-module.EnableCritsOnly.Hint": "Players will only hit if they crit, and otherwise miss automatically *manic laughter*"
 * }
 *
 * The naming scheme is:
 * {
 *  "[MODULE_NAME].[SETTING_NAME].Name": "[TEXT]",
 *  "[MODULE_NAME].[SETTING_NAME].Hint": "[TEXT]"
 * }
 */
const settings = [

  {
    name: "ItemRollButtons",
    scope: "client",
    default: true,
    type: Boolean,
    onChange: fetchParams
  },
  {
    name: "SpeedItemRolls",
    scope: "client",
    default: true,
    type: Boolean,
    onChange: (value) => {window.location.reload()}
  },
  {
    name: "AutoTarget",
    scope: "world",
    default: "None",
    type: String,
    choices: {none: "None", always: "Always", wallsBlock: "Walls Block"},
    onChange: fetchParams
  },
  {
    name: "UseHigherSlot",
    scope: "client",
    default: true,
    type: Boolean,
    onChange: fetchParams
  },
  {
    name: "AutoCheckHit",
    scope: "world",
    default: true,
    type: Boolean,
    onChange: fetchParams
  },
  {
    name: "AutoCheckSaves",
    scope: "world",
    default: true,
    type: Boolean,
    onChange: fetchParams
  },
  {
    name: "AutoRollDamage",
    scope: "world",
    default: true,
    type: Boolean,
    onChange: fetchParams
  },
  {
    name: "AddChatDamageButtons",
    scope: "world",
    default: true,
    type: Boolean,
    onChange: fetchParams
  },
  {
    name: "AutoApplyDamage",
    scope: "world",
    default: true,
    type: Boolean,
    onChange: fetchParams
  },
  {
    name: "DamageImmunities",
    scope: "world",
    default: true,
    type: Boolean,
    onChange: fetchParams
  },
  {
    name: "MacroSpeedRolls",
    scope: "client",
    default: true,
    type: Boolean,
    onChange: fetchParams
  },
  {
    name: "HideNPCNames",
    scope: "world",
    default: true,
    type: Boolean,
    onChange: fetchParams
  },
  {
    name: "ItemDeleteCheck",
    scope: "client",
    default: true,
    type: Boolean,
    choices: [],
    onChange: fetchParams
  },
]

export const registerSettings = () => {
  settings.forEach(setting => {
    let options = {
        name: game.i18n.localize(`${MODULE_NAME}.${setting.name}.Name`),
        hint: game.i18n.localize(`${MODULE_NAME}.${setting.name}.Hint`),
        scope: setting.scope,
        config: true,
        default: setting.default,
        type: setting.type,
        onChange: setting.onChange,
        choices: null
    };
    if (setting.choices) options.choices = setting.choices;
    game.settings.register(MODULE_NAME, setting.name, options);
  });
}